from django.shortcuts import render, redirect
from receipts.models import ExpenseCategory, Account, Receipt,ExpenseCategory
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, AccountForm, ExpenseCategoryForm
# Create your views here.
@login_required
def list_view(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts": receipts,
    }
    return render (request, "receipts/list.html", context)

@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            form = form.save(commit=False)
            form.purchaser = request.user
            form.save()
            return redirect("home")

    else:
        form = ReceiptForm()

    context = {
            "form": form
        }
    return render(request, "receipts/create.html", context)

@login_required
def category_list(request):
    expenses = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "expenses": expenses,
    }
    return render(request, "categories/list.html", context)

@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "accounts": accounts,
    }
    return render(request, "accounts/list.html", context)

@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            form = form.save(commit=False)
            form.owner = request.user
            form.save()
            return redirect("category_list")

    else:
        form = ExpenseCategoryForm()

    context = {
            "form": form
        }
    return render(request, "categories/create.html", context)

@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            form = form.save(commit=False)
            form.owner = request.user
            form.save()
            return redirect("account_list")

    else:
        form = AccountForm()

    context = {
            "form": form
        }
    return render(request, "accounts/create.html", context)
